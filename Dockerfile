FROM docker.io/node:alpine

RUN apk --no-cache add hugo git git-lfs gzip imagemagick

RUN npm install -g npm@latest --cache=/tmp/npm && \
    npm install -g netlify-cli@latest --cache=/tmp/npm && \
    rm -rf /tmp/*
